# Utilisation
# 'make' ou 'make all' -> création de l'exécutable
# 'make clean' -> suppression des fichiers temporaires
# 'make doc' -> création de la doc

# Définition des variables
CC = gcc-4.9
CFLAGS = -Wall -W -g -std=c99
NBSAMPLES = 11 

.PHONY: doc

all: 
	$(CC) src/*.c src/*.h -o simplex $(CFLAGS)		
	
clean:
	rm -rf *.o *~

tex:
	cd report && pdflatex report.tex && bibtex report.aux && pdflatex report.tex && pdflatex report.tex

doc:	
	doxygen make_doc && cd doc/latex && make

really_all:
	make
	make tex
	make doc

tests:
	cat res/tests/answers && for i in $$(seq 1 $(NBSAMPLES)); do cat res/tests/sample$$i |./simplex|grep 'optimum'|grep -o [\-\.0-9]*; done
