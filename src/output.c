#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "types.h"

/**
 * \file output.c
 * \brief A file to display the structures in types.h
 */

/**
 * \brief Print an obj
 * 
 * \param objective The obj to be printed. 
 */
void print_objective(const obj objective) {
	printf("Objective: %s(", (objective.max) ? "max" : "min");
	printf("%lf", objective.offset);
	
	for(int i = 0; i < objective.size; i++)
		printf(" + %lf*x%d", objective.coeffs[i], i);
	printf(")\n");
}

/**
 * \brief Print a constraints
 * 
 * \param cons The constraints to be printed. 
 */
void print_constraints(const constraints cons) {
	int n = cons.n, m = cons.m;

	printf("Constraints:\n");

	for(int i = 0; i < m; i++) {
		printf("%lfx0", cons.matrix[i][0]);
		for(int j = 1; j < n; j++)
			printf(" + %lfx%d", cons.matrix[i][j], j);

		// Print the sign
		if(cons.type[i] == INFERIOR)
			printf(" <= ");
		if(cons.type[i] == EQUAL)
			printf(" = ");
		if(cons.type[i] == SUPERIOR)
			printf(" >= ");

		printf("%lf\n", cons.matrix[i][n]);
	}
}


/**
 * \brief Print a tableau
 * 
 * \param tab The tableau to be printed
 */
void print_tableau(const tableau tab) {
	const int n = tab.n, m = tab.m;

	for(int i = 0; i < (m + 1); i++) {
		for(int j = 0; j < (n + m + 1); j++)
			printf("%lf\t", tab.matrix[i][j]);
		printf("\n");
	}
	printf("The basis is: ");
	for(int i = 0; i < m; i++)
		printf("x%d ", tab.basis[i]);
	printf("\n");
}

/**
 * \brief Print the solution
 *
 * \param solution The sol to be printed
 */
void print_sol(const sol solution) {
	if(solution.type == UNBOUNDED) {
		printf("The solution is unbounded");
		return;
	}

	if(solution.type == INFEASIBLE) {
		printf("There is no solution");
		return;
	}

	assert(solution.type == OPTIMUM);
	
	printf("The optimum solution is: %lf\n\n", solution.value);
	
	printf("The variables are:\n");
	for(int i = 0; i < solution.size; i++)
		printf("x%d = %lf\n", i, solution.obfs[i]);
}
