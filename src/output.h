#ifndef OUTPUT_H
#define OUTPUT_H

#include "types.h"

/**
 * \file output.h
 * \brief A file to display structures of types.h
 */


void print_objective(const obj objective);
void print_constraints(const constraints cons);
void print_tableau(const tableau tab);
void print_sol(const sol solution);

#endif

