#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "types.h"
#include "free.h"

// FIXME
#include "output.h"

/**
 * \file simplex.c
 * \brief A file to store the simplex method
 */

#define INVALID_PIVOT -1
#define NONBASIC -1
#define EPSILON 10e-10

#define abs(x) (((x) > 0) ? (x) : -(x))

/**
 * \brief Tranforms the obj into a standart obj (min)
 * \param objective The obj to transform
 */
void make_standart_obj(obj *objective) {
	if(!objective->max)
		return;

	objective->max = false;
	for(int i = 0; i < objective->size; i++)
		objective->coeffs[i] *= -1.;
}

/**
 * \brief Transforms the cons into a standart cons (only inferiror)
 * \param cons The cons to transform
 */
void make_standart_cons(constraints *cons) {
	int nb_equal = 0;
	for(int i = 0; i < cons->m; i++)
		if(cons->type[i] == EQUAL)
			nb_equal += 1;
	cons->m += nb_equal;

	if(nb_equal != 0) {
		cons->matrix = realloc(cons->matrix, cons->m * sizeof(double*));
		for(int i = cons->m - nb_equal; i < cons->m; i++)
			cons->matrix[i] = malloc((cons->n + 1) * sizeof(double));
		
		cons->type = realloc(cons->type, cons->m * sizeof(int));
	}


	int i_equal = cons->m - 1;
	for(int i = 0; i < cons->m - nb_equal; i++) {
		if(cons->type[i] == SUPERIOR) {
			for(int j = 0; j < cons-> n + 1; j++)
				if(abs(cons->matrix[i][j]) >= EPSILON) // To avoid -0.000000
					cons->matrix[i][j] *= -1.;
			cons->type[i] = INFERIOR;
		} else if(cons->type[i] == EQUAL) {
			for(int j = 0; j < cons-> n + 1; j++)
                                if(abs(cons->matrix[i][j]) >= EPSILON) // To avoid -0.000000
	                                cons->matrix[i_equal][j] = -1. * cons->matrix[i][j];
			cons->type[i] = INFERIOR;
			cons->type[i_equal] = INFERIOR;
			i_equal--;
		} else
			assert(cons->type[i] == INFERIOR);
	}
}



/**
 * \brief Make a tableau from an obj and a constraints
 * 
 * \param[in] objective The objective function
 * \param[in] constraints The constraints of the system
 * \param[out] tab The tableau made from the obj and the constraints
 *
 * This function make a tableau from an obj and a constraints. All the constraints in the
 * tableau are now in a standart form. The objective function must be minimized.
 */
void make_tableau(const obj objective, const constraints cons, tableau *tab) {
	assert(objective.size == cons.n);
	assert(!objective.max);

	int n = cons.n, m = cons.m;

	tab->n = n;
	tab->m = m;

	// Malloc the matrix and the basis
	tab->basis  = malloc(m * sizeof(double));
	tab->matrix = malloc((m + 1) * sizeof(double*));
	for(int i = 0; i < (m + 1); i++)
		tab->matrix[i] = malloc((n + m + 1) * sizeof(double));

	/* Fill the tableau */
	// Objective
	for(int i = 0; i < n; i++)
		tab->matrix[0][i] = objective.coeffs[i];
	tab->matrix[0][n + m] = 0;

	// Constraints
	for(int i = 1; i < (m + 1); i++) {
		for(int j = 0; j < n; j++)
			tab->matrix[i][j] = cons.matrix[i-1][j];
		tab->matrix[i][n + m] = cons.matrix[i-1][n];
	}

	// Slack variables
	for(int i = 1; i < (m + 1); i++)
		for(int j = n; j < (n + m); j++)
			tab->matrix[i][j] = ((i-1) == (j-n)) ?  1. : 0.;

	// Basis
	for(int i = 0; i < m; i++)
		tab->basis[i] = n + i;
}

/**
 * \brief Pick the entering variable
 *
 * \param[in] tab The tableau of the simplex
 * \return The indice of the entering variable
 * 
 * The entering variable is the variable which minimises the most the objective function.
 */
int pick_entering(const tableau tab) {
	int i;
	for(i = 0; i < (tab.n + tab.m); i++)
		if(tab.matrix[0][i] < -EPSILON)
			break;

	return i;
}

/**
 * \brief Pick the pivot line
 *
 * \param[in] tab The tableau of the simplex
 * \param[in] entering The indice of the entering varaible
 * \return The indice of the line which is the pivot. Return -1 if there is no pivot (unbounded).
 *
 * This function selects the pivot by minimising the ration.
 * One of the element of the pivot column (entering variable) must be non negative !
 */
int pick_pivot(const tableau tab, const int entering) {
	int last_column = tab.n + tab.m;
	assert((entering >= 0) && (entering < (tab.n + tab.m)));

	// Search for a nonnegative value in the pivot column
	int pivot = -1;
	for(int i = 1; i < (tab.m + 1); i++)
		if(tab.matrix[i][entering] > EPSILON) {
			pivot = i;
			break;
	}
	if(pivot == -1)
		return INVALID_PIVOT;

	// Look for the min ratio
        double ratio_min = tab.matrix[pivot][last_column]/tab.matrix[pivot][entering];
	for(int i = pivot + 1; i < (tab.m + 1); i++) {
		if(tab.matrix[i][entering] > EPSILON) {
			double ratio = tab.matrix[i][last_column]/tab.matrix[i][entering];
			if((ratio < ratio_min) || (ratio == ratio_min && tab.basis[i] < tab.basis[pivot])) {
				ratio_min = ratio;
				pivot = i;
			}
		}
	}
	
	return pivot;
}

/**
 * \brief Multiply a line of the tableau
 *
 * \param[in, out] tab The tableau whose line is multiplied
 * \param[in] line The indice of the line to multiply
 * \param[in] coeff The coeff to multply the line
 */
void multiply(tableau *tab, const int line, const double coeff) {
	for(int i = 0; i < (tab->n + tab->m + 1); i++)
		tab->matrix[line][i] *= coeff;
}

/**
 * \brief Apply the pivot to the tableau
 * 
 * \param[in, out] tab The tableau of the simplex
 * \param[in] pivot The indice of the pivot
 * \param[in] entering The indice of the entering value
 *
 * We apply the pivot to all the lines of the tableau.
 * We got now a new basis.
 */
void apply_pivot(tableau *tab, const int pivot, const int entering) {
	for(int i = 0; i < (tab->m + 1); i++) {
		if(i != pivot) {
			double coeff = tab->matrix[i][entering];
			for(int j = 0; j < (tab->m + tab->n + 1); j++)
				tab->matrix[i][j] -= coeff*tab->matrix[pivot][j];
		}
	}
}

/**
 * \brief Set the obfs from the tableau
 * \param[in] tab The tableau of the simplex
 * \param[out] solution The sol where the obfs will be set
 *
 * solution.size is now set to tab.n + tab.m and the solution must now be freed.
 */
void get_obfs(const tableau tab, sol *solution) {
	solution->size = tab.n + tab.m;
	solution->obfs = malloc(solution->size * sizeof(double));

	for(int i = 0; i < solution->size; i++)
		solution->obfs[i] = 0.;

	for(int j = 0; j < tab.m; j++)
		solution->obfs[tab.basis[j]] = tab.matrix[j + 1][tab.n + tab.m];
}

/**
 * \brief Return true if the tableau is feasible

 * \param tab The tableau to check
 * \return True if the tableau is feasible
 */
bool is_feasible(const tableau tab) {
	for(int i = 1; i < (tab.m + 1); i++) {
		if(abs(tab.matrix[i][tab.n + tab.m]) < EPSILON)
			tab.matrix[i][tab.n + tab.m] = 0.;
		if(tab.matrix[i][tab.n + tab.m] < 0)
			return false;
	}

	return true;
}

/**
 * \brief Return true if the tableau is minimized
 *
 * \param[in] tab The tableau of the simplex
 * \return true if the tableau is minimized
 * 
 * The tableau is minimized if all the the coeffs of the objective function are postive.
 */
bool is_minimized(const tableau tab) {
	for(int i = 0; i < (tab.n + tab.m); i++)
		if(tab.matrix[0][i] < -EPSILON)
			return false;
	return true;
}

/**
 * \brief Return true if the variable checked is basic
 * 
 * \param i_var The indice of the variable to checked (1 means x1)
 * \param tab The tableau of the simplex
 * \return true if i_var is basic
 */
bool is_basic(int i_var, const tableau tab) {
	for(int i = 0; i < tab.m; i++)
		if(tab.basis[i] == i_var)
			return true;
	return false;
}

/**
 * \brief Return the line if the variable checked is basic
 * 
 * \param i_var The indice of the variable to checked (1 means x1)
 * \param tab The tableau of the simplex
 * \return the line of i_var as basic
 */
int get_basic_line(int i_var, const tableau tab) {
        for(int i = 0; i < tab.m; i++)
                if(tab.basis[i] == i_var)
                        return i + 1; // +1 because of the objective function in the tableau
        return NONBASIC;
}


/**
 * \brief Minimize the objective function if the tableau is feasible
 * 
 * \param tab The taleau of the simplex
 * \return The solution of the algorithm 
 */
sol solve_feasible(tableau tab) {
	assert(is_feasible(tab));
	
	sol solution;
	
	while(!is_minimized(tab)) {
		int entering = pick_entering(tab);
		int pivot = pick_pivot(tab, entering);
		if(pivot == INVALID_PIVOT) {
			solution.type = UNBOUNDED;
			return solution;
		}

		tab.basis[pivot-1] = entering;
		// Normalise the pivot
		multiply(&tab, pivot, 1./tab.matrix[pivot][entering]);
		apply_pivot(&tab, pivot, entering);
	}

	get_obfs(tab, &solution);	
	solution.type  = OPTIMUM; 
	solution.value = - tab.matrix[0][tab.n + tab.m];
	return solution;
}

/**
 * \brief Make an initial feasible basis to apply the simplex
 *
 * \param[in, out] tab A tableau to make feasible
 * \return True if the tableau was made feasible
 */
bool make_initial(tableau *tab) {
	int n = tab->n, m = tab->m;

	if(is_feasible(*tab))
		return true;

	// Save and erase the objective function
	double *obj = malloc((n + m + 1) * sizeof(double));
	for(int i = 0; i < n + m + 1; i++) {
		obj[i] = tab->matrix[0][i];
		tab->matrix[0][i] = 0.;
	}
	
	// Add an extra variable
	for(int i = 0; i < m + 1; i++) {
		tab->matrix[i] = realloc(tab->matrix[i], (n + m + 2)*sizeof(double));
		tab->matrix[i][n + m + 1] = tab->matrix[i][n + m];
		tab->matrix[i][n + m] = -1.;
	}
	tab->n += 1; n += 1;
	
	// Set the new objective function
	tab->matrix[0][n + m - 1] = 1.; // Minimize the added variable

	int entering = n + m - 1; // The added variable
	// Search for the most infeasible
	int pivot = 1;
	for(int i = 2; i < m + 1; i++)
		if(tab->matrix[i][n + m] < tab->matrix[pivot][n + m])
			pivot = i;
	assert(tab->matrix[pivot][n + m] < 0.);

	// Pivot to make it (n'eisti) feasible
        tab->basis[pivot-1] = entering;
	multiply(tab, pivot, 1./tab->matrix[pivot][entering]);
	apply_pivot(tab, pivot, entering);

	sol solution = solve_feasible(*tab);

	if(abs(solution.value) >= EPSILON)
		return false;

	
	pivot = get_basic_line(n + m - 1, *tab);
	if(pivot != NONBASIC) { // The added variable is basic
		/* Perform a degenerated pivot */
		// Pick any entering (non basic) where the coeff will be non zero
		int entering = -1;
		for(int i = 0; i < n + m; i++)
			if(!is_basic(i, *tab) && tab->matrix[pivot][i] != 0.) {
				entering = i;
				break;
			}

		// Degenerated pivot
		tab->basis[pivot-1] = entering;
	        multiply(tab, pivot, 1./tab->matrix[pivot][entering]);
		apply_pivot(tab, pivot, entering);
	}
	
	// Remove the extra variable
	for(int i = 0; i < m + 1; i++) {
		tab->matrix[i][n + m - 1] = tab->matrix[i][n + m];
		tab->matrix[i] = realloc(tab->matrix[i], (n + m)*sizeof(double));
	}
	tab->n -= 1; n -= 1;

	// Reset the objective function
	for(int i = 0; i < n + m + 1; i++)
		tab->matrix[0][i] = 0;
	for(int i = 0; i < n + m + 1; i++) {
		int line = get_basic_line(i, *tab);
		if(line == NONBASIC)
			tab->matrix[0][i] += obj[i];
		else // Replace the basic variable by its value from others non variables
			for(int j = 0; j < n + m + 1; j++) //FIXME look at it more carefully
				if(i != j && !is_basic(j, *tab)) {
					if(j != n + m)
						tab->matrix[0][j] -= obj[i] * tab->matrix[line][j];
					else
						tab->matrix[0][j] -= obj[i] * tab->matrix[line][j];
				}
		
	}
	free(obj);

	return true;
}

/**
 * \brief Solve the linear optimisation problem by the method of the simplex
 *
 * \param objective An objective function
 * \param cons A set of constraints
 * \return The solution of the linear optimisation problem
 *
 * The objective and the cons is freed after the simplex
 */
sol simplex(obj objective, constraints cons) {
	sol solution;
	float max_coeff = (objective.max) ? -1. : 1.;

	make_standart_obj(&objective);
	make_standart_cons(&cons);

	tableau tab;
	make_tableau(objective, cons, &tab);

	if(!make_initial(&tab)) {
		solution.type = INFEASIBLE;
		return solution;
	}

	solution = solve_feasible(tab);
	solution.value *= max_coeff;
	solution.value += objective.offset;

	free_tableau(tab);
	free_obj(objective);
        free_constraints(cons);	

	return solution;
}
