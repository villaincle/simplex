#include <stdlib.h>
#include "types.h"

/**
 * \file free.c
 * \brief A file to free structures of types.h
 */

/**
 * \brief Free an obj
 *
 * \param objective The obj to be freed. 
 */
void free_obj(obj objective) {
	free(objective.coeffs);
}

/**
 * \brief Free a constraints
 *
 * \param cons The constraints to be freed. 
 */
void free_constraints(constraints cons) {
	for(int i = 0; i < cons.m; i++)
		free(cons.matrix[i]);
	free(cons.matrix);
	free(cons.type);
}

/**
 * \brief Free a tableau
 *
 * \param tab The tableau to be freed. 
 */
void free_tableau(tableau tab) {
	for(int i = 0; i < (tab.m + 1); i++)
		free(tab.matrix[i]);
	free(tab.matrix);
	free(tab.basis);
}

/**
 * \brief Free a sol
 *
 * \param solution The sol to be freed.
 */
void free_sol(sol solution) {
	if(solution.type == OPTIMUM)
		free(solution.obfs);
}

