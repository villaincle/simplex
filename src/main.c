#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "types.h"
#include "input.h"
#include "output.h"
#include "simplex.h"
#include "free.h"

/**
 * TODO: doc
 */
int main(void) {
	obj objective;
	constraints cons;
	sol solution;

	if(!read_input(&objective, &cons)) {
		printf("Error while reading input");
		return EXIT_FAILURE;
	}

	print_objective(objective);
	print_constraints(cons);

	solution = simplex(objective, cons);
	print_sol(solution);

	free_sol(solution);
		
	return EXIT_SUCCESS;
}
