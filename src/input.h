#ifndef INPUT_H
#define INPUT_H

#include <stdbool.h>
#include "types.h"

/**
 * \file input.h
 * \brief A file header to read input
 */


bool read_input(obj *objective, constraints *cons);

#endif

