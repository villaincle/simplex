#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "types.h"
#include "free.h"

/**
 * \file input.c
 * \brief A file to read the input of the user 
 */

/**
 * \brief Read the input of the user
 * 
 * \param[out] objective The obj where the input will be stored.
 * \param[out] cons The constraints where the input will be stored.
 * \return true if the function is a success.
 *
 * The input must be in this order:
 * n m, max (0 or 1), the offset, the coeffs of the objective, the coeffs of the constraints. 
 * At the end of each contraints there must be the inequality constant.
 * Exemple: 1 2 3 4 could means 1*x0 + 2*x2 + 3*x3 <= 4.
 * If the return value is false, the obj and the constraints do not need to be freed.
 * 
 * Samples of input can be found at res/sample*
 */
bool read_input(obj *objective, constraints *cons) {
	int n,  m;
	int max;

	if(scanf("%d %d", &n, &m) != 2)
		return false;

	if(scanf("%d", &max) != 1)
		return false;
	objective->max = (bool) max;

	if(scanf("%lf", &(objective->offset)) != 1)
		return false; 

	// Fill the coeffs of the objective function
	objective->coeffs = malloc(n * sizeof(double));
	for(int i = 0; i < n; i++)
		if(scanf("%lf", &(objective->coeffs[i])) != 1) {
			free_obj(*objective);
			return false;
		}

	objective->size = n;
	cons->n = n;
	cons->m = m;
	
	// Fill the matrix of the constraints
	cons->type = malloc(m * sizeof(int));
	cons->matrix = malloc(m * sizeof(double*));
	for(int i = 0; i < m; i++) {
		cons->matrix[i] = malloc((n + 1) * sizeof(double));
		scanf("%d", &(cons->type[i]));
		assert(cons->type[i] <= 2 && cons->type[i] >= 0);
		for(int j = 0; j < n + 1; j++)
			if(scanf("%lf", &(cons->matrix[i][j])) !=  1) {
				free_obj(*objective);
				free_constraints(*cons);
				return false;
			}
	}

	return true;
}
