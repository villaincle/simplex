#ifndef FREE_H
#define FREE_H

#include "types.h"

/**
 * \file free.h
 * \brief A file header to free structures of types.h
 */

void free_obj(obj objective);
void free_constraints(constraints cons);
void free_tableau(tableau tab);
void free_sol(sol solution);

#endif

