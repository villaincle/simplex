#ifndef SIMPLEX_H
#define SIMPLEX_H

#include <stdbool.h>
#include "types.h"

/**
 * \file simplex.h
 * \brief File header of simplex.c
 */


void make_standart_obj(obj *objective);
void make_standart_cons(constraints *cons);
void make_tableau(const obj objective, const constraints cons, tableau *tab);
int pick_entering(const tableau tab);
int pick_pivot(const tableau tab, const int entering);
void multiply(tableau *tab, const int line, const double coeff);
void apply_pivot(tableau *tab, const int pivot, const int entering);
void get_obfs(const tableau tab, sol *solution);
bool is_feasible(const tableau tab);
bool is_minimized(const tableau tab);
sol solve_feasible(tableau tab);
bool make_initial(tableau *tab);
sol simplex(obj objective, constraints cons);

#endif

