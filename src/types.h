#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>

#define OPTIMUM		0
#define UNBOUNDED	1
#define INFEASIBLE	2

#define INFERIOR	0
#define EQUAL		1
#define SUPERIOR	2

/**
 * \file type.h
 * \brief Structs of the project simplex
 */


/**
 * \strcut obj
 * \brief Objective function
 * 
 * This structure is used to store the input before it is transformed into
 * a standart problem.
 * Exemple with the objective function being max(2 + x1 + 3*x2):
 *	 2 is the offset,
 *	[1, 3] are the coeffs,
 *	size = 2 and max = true.
 */
struct obj {
	int size;	/**< size is the number of variables. */
	double offset;  /**< offset is the offset of the objective function. */
	double *coeffs; /**< coeffs is the array of the coefficients of the objective function. */
	bool max;	/**< max is a flag to know if the objective function must be maxed or minimized. */
};
typedef struct obj obj;

/**
 * \struct constraints
 * \brief Constraints
 *
 * This structure is used to store the input before it is transformed into a standart problem.
 * There is no slack variable in constraints.
 * matrix must be allocated in order to be used. Please use the size indicated to allocate it.
 */
struct constraints {
	int n;			/**< n is the number of variables of the objective function. */
	int m;			/**< m is the number of constraints. */
	int *type;		/**< type is use to store the type of the contraint.
				 * 0 means inferior, 1 means equal and 2 means superior */
	double **matrix;	/**< matrix is use to store the constraint. Its size is 
				 * (n + 1) columns and m rows. matrix[0] is the first row. */
};
typedef struct constraints constraints;

/**
 * \struct tableau
 * \brief Simplex tableau
 *
 * The tableau is used to optimize the objective function with the simplex method. 
 * It is in the standart form. There are slack variables in the tableau.
 */ 
struct tableau {
	int n;			/**< n is the number of variables of the objective function. */
	int m;			/**< m is the number of constraints. */
	int *basis;		/**< basis is used to store which variables is the basis.
				 * The basis indice is the line of the tableau (minus the line of the obj)
				 * The basis value is the indice of the variable */
	double **matrix;	/** The size of the matrix is (n + m + 1) columns and (m + 1) rows.
				 * matrix[0] is the first row and it represents the objective function. */
};
typedef struct tableau tableau;

/**
 * \struct sol
 * \brief Solution of a linear problem
 *
 * This structure is used to return the solution of the simplex algorithm.
 */
struct sol {
	short type;	/**< type is either OPTIMUM, UNBOUNDED or INFEASIBLE. */
	double value;	/**< value is the value of the maxed or minimised objective function. */
	double *obfs;	/**< obfs stands for optimmium basic feasible solution. */
	int size;	/**< size is the size of the obfs */

};
typedef struct sol sol;

#endif

